import 'package:meta/meta.dart';
import 'package:json_annotation/json_annotation.dart';

part 'placeholders.g.dart';

@JsonSerializable()
class Placeholders {
  @JsonKey(name: 'albumId')
  final int albumId;
  @JsonKey(name: 'id')
  final int id;
  @JsonKey(name: 'title')
  final String title;
  @JsonKey(name: 'url')
  final String url;
  @JsonKey(name: 'thumbnailUrl')
  final String thumbnailUrl;
  

  const Placeholders({
    @required this.id,
    @required this.albumId,
    @required this.title,
    @required this.url,
    @required this.thumbnailUrl
  });

  
  factory Placeholders.fromJson(Map<String, dynamic> json) => _$PlaceholdersFromJson(json);
  
  Map<String, dynamic> toJson() => _$PlaceholdersToJson(this);

  Placeholders copyWith({
    int id,
    int albumId,
    String title,
    String url,
    String thumbnailUrl
  }) => Placeholders(
    id: id ?? this.id, 
    albumId: albumId ?? this.albumId, 
    title: title ?? this.title, 
    url: url ?? this.url,
    thumbnailUrl: thumbnailUrl ?? this.thumbnailUrl
  );

  
}
