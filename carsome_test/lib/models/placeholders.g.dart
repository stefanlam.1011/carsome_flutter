// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'placeholders.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Placeholders _$PlaceholdersFromJson(Map<String, dynamic> json) {
  return Placeholders(
    id: json['id'] as int,
    albumId: json['albumId'] as int,
    title: json['title'] as String,
    url: json['url'] as String,
    thumbnailUrl: json['thumbnailUrl'] as String,
  );
}

Map<String, dynamic> _$PlaceholdersToJson(Placeholders instance) =>
    <String, dynamic>{
      'albumId': instance.albumId,
      'id': instance.id,
      'title': instance.title,
      'url': instance.url,
      'thumbnailUrl': instance.thumbnailUrl,
    };
