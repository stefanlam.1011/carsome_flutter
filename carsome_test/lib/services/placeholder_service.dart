import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import 'package:carsome_test/services/services.dart';
import 'package:http/http.dart' as http;
import 'package:carsome_test/models/models.dart';
import 'package:carsome_test/services/services.dart';

class PlaceholderService extends Service {
  static const albumEndpoint = 'photos?albumId=';
  

  PlaceholderService({
    @required http.Client httpClient
  })
    : assert(httpClient != null),
      super(httpClient: httpClient);

  Future <List<Placeholders>> getPlaceholderList(String pageNum) async {
    try {

      final http.Response response = await get(endpoint: albumEndpoint + pageNum);
      var jsonDecoded = jsonDecode(response.body);
      List<Placeholders> list = (jsonDecoded as List)
      .map((data) => new Placeholders.fromJson(data))
      .toList();
      
      return list;
    } catch (e, s) {
      print('$runtimeType getPlaceholder error $e\n$s');
      return Future.error(e);
    }
  }



}
