import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:meta/meta.dart';




class Service {
  static const String baseUrl = 'https://jsonplaceholder.typicode.com';
  http.Client httpClient;

  Service({@required this.httpClient}) : assert(httpClient != null);

  
  Future get({
    @required String endpoint,
    Map<String, String> headers,
  }) async {
    
    print('url = $baseUrl/$endpoint');
    final http.Response httpResponse = await httpClient.get('$baseUrl/$endpoint');
    print('get http status: ${httpResponse.statusCode} ${httpResponse.reasonPhrase}');
    
    //print('get responsebody: ${httpResponse.body}');
    return httpResponse;
  }

  
}
