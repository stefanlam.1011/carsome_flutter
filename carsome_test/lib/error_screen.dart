import 'package:flutter/material.dart';

class ErrorScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Padding(
        padding:const EdgeInsets.all(8.0),
        child:Text(
        'Sorry, something went wrong there. Please try again.'
      ),
      )
    );
  }
}