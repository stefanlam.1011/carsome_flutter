import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

import 'package:carsome_test/models/placeholders.dart';
import 'package:carsome_test/repository/app_repository.dart';
import 'package:flutter/material.dart';
import './bloc.dart';



class AppBloc extends Bloc<AppEvent, AppState> {
  
  final AppRepository appRepository;

  AppBloc({
    @required this.appRepository,
  }) : assert(appRepository != null);

  @override
  AppState get initialState => AppInitial();
  
  @override
  Stream<AppState> mapEventToState(
    AppEvent event,
  ) async* {
    if (event is FetchAlbum) {
      yield AppLoading();
      try { 
        final List<Placeholders> placeholders = await appRepository.getPlaceholder(event.pageNum.toString())
          .catchError((e) => throw e);

        print('yield placeholder loaded');
        yield AppLoaded(placeholderList: placeholders);
      } catch (e) {
        print('${event.runtimeType} error $e');
        yield AppError(error: e);
      }
    }
  }
}
