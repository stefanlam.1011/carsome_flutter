
abstract class AppEvent {}

class FetchAlbum extends AppEvent {
  final int pageNum;
  FetchAlbum(this.pageNum);
}