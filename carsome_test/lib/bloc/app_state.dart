import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';
import 'package:carsome_test/models/models.dart';

abstract class AppState extends Equatable
{
  const AppState();

  @override
  List<Object> get props => [];
}

class AppInitial extends AppState {}


class AppNotLoaded extends AppState {}

class AppLoading extends AppState {}

class AppLoaded extends AppState {
  final List<Placeholders> placeholderList;

  AppLoaded({
    @required this.placeholderList,
  }) : assert(placeholderList != null);

  @override
  List<Object> get props {
    print('prop getter for app state');
    return [placeholderList];
  }
}

class AppError extends AppState {
  final dynamic error;
  
  const AppError({this.error});

  @override
  List<Object> get props => [error];
}