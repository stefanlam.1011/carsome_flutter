import 'package:carsome_test/empty_screen.dart';
import 'package:carsome_test/error_screen.dart';
import 'package:carsome_test/services/placeholder_service.dart';
import 'package:flutter/material.dart';
import 'package:transparent_image/transparent_image.dart';
import 'package:carsome_test/extensions.dart';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:carsome_test/bloc/bloc.dart';
import 'package:carsome_test/repository/app_repository.dart';
import 'package:carsome_test/models/models.dart';
import 'package:http/http.dart' as http;

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  List<bool> isSelected;
  final http.Client httpClient = http.Client();
  
  @override
    void initState() {
        super.initState();
        isSelected = [true,false];
    }
    
  @override
  Widget build(BuildContext context) {
    
    return MultiRepositoryProvider(
              providers: <RepositoryProvider>[
                RepositoryProvider<AppRepository>(
                  create: (_) => AppRepository(placeholderService: PlaceholderService(httpClient: httpClient)),
                ),
              ], 
              child:BlocProvider<AppBloc>(
              create: (context) => AppBloc(
                appRepository: RepositoryProvider.of<AppRepository>(context),
              )..add(FetchAlbum(1)),//initially page 1
              child: BlocListener<AppBloc, AppState>(
                  listener: (_, state) {
                    if (state is AppLoaded) {
                      
                    }
                  },
              child:Scaffold(
              appBar: AppBar(
                // Here we take the value from the MyHomePage object that was created by
                // the App.build method, and use it to set our appbar title.
                title: Text(widget.title),
              ),
              body: BlocBuilder<AppBloc, AppState>(
                builder: (context, state) {
                      return Center(
                      // Center is a layout widget. It takes a single child and positions it
                      // in the middle of the parent.
                      child: OrientationBuilder(
                      builder: (context, orientation) {
                        return Column(
                            children: <Widget>[
                              SizedBox(
                                height: 12.0
                              ),
                              ToggleButtons(
                                borderColor: Colors.white,
                                fillColor: HexColor.fromHex('#E0EEFA'),
                                borderWidth: 2,
                                selectedBorderColor: Colors.blue,
                                selectedColor: Colors.white,
                                borderRadius: BorderRadius.circular(0),
                                children: <Widget>[
                                    Container(
                                      color:  Colors.black,
                                      padding: const EdgeInsets.symmetric(horizontal: 6.0, vertical: 4.0),
                                      child:Text(
                                        '1',
                                        style: TextStyle(fontSize: 11, color: Colors.white),
                                      )
                                    ),
                                    Container(
                                      color:  Colors.black,
                                      padding: const EdgeInsets.symmetric(horizontal: 6.0, vertical: 4.0),
                                      child:Text(
                                        '2',
                                        style: TextStyle(fontSize: 11, color: Colors.white),
                                      )
                                    ),
                                    
                                ],
                                onPressed: (int index) {
                                  int pageNum = index + 1;
                                  setState(() {
                                    for (int i = 0; i < isSelected.length; i++) {
                                        isSelected[i] = i == index;
                                    }
                                  });

                                  BlocProvider.of<AppBloc>(context).add(FetchAlbum(pageNum));
                                },
                                isSelected: isSelected,
                              ),
                              SizedBox(
                                height: 12.0
                              ),
                              _buildGridAlbum(orientation, state)
                              
                              
                            ]);
                        
                        }
                      )
                    );

                    
                
                
                })
                
            )
              )
            )
    );
  }

  Widget _buildGridAlbum(Orientation orientation, AppState state) {
    if (state is AppLoading) {
        return Center(
             child: Padding(
               padding: const EdgeInsets.all(28.0),
               child: CircularProgressIndicator(),
             ),
        );
    }

    List<Placeholders> albumList;
    if(state is AppLoaded){
      albumList = state.placeholderList; 
    }

    if(albumList.length > 0){
      return Expanded(
                                flex: 1,
                                child:Container(
                                          margin: const EdgeInsets.symmetric(horizontal: 28.0),
                                          child:GridView.builder(
                                          itemBuilder: (context, position) {
                                            if (state is AppLoaded) {
                                                
                                                return 
                                                    Container(
                                                        margin: const EdgeInsets.symmetric(horizontal: 10.0),
                                                        child:Column(
                                                        children: <Widget>[
                                                            FadeInImage.memoryNetwork(
                                                              placeholder: kTransparentImage,
                                                              image: albumList[position].thumbnailUrl,
                                                              fit: BoxFit.contain
                                                            ),
                                                        Text(albumList[position].title, overflow: TextOverflow.ellipsis, style: TextStyle(fontSize: orientation == Orientation.portrait ? 12.0 : 9.0))
                                                        ],
                                                        )
                                                                
                                                  );

                                            }
                                            return ErrorScreen();
                                            
                                                
                                      },
                                      itemCount: 12,
                                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: orientation == Orientation.portrait ? 2 : 5),    
                                  )
                                )
                              );
    }
    else{
      return EmptyScreen();
    }
    
  
  }

  
}
