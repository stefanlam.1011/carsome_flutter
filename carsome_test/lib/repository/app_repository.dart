import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import 'package:carsome_test/models/models.dart';
import 'package:carsome_test/services/services.dart';

class AppRepository {
  final PlaceholderService placeholderService;

  AppRepository({@required this.placeholderService})
    : assert(placeholderService != null);

  Future <List<Placeholders>> getPlaceholder(String pageNum) async {
    return placeholderService.getPlaceholderList(pageNum);
  }
}